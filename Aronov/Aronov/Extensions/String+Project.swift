//
//  File.swift
//  Aronov
//
//  Created by Eugene Aronov on 14.10.16.
//  Copyright © 2016 Eugene Aronov. All rights reserved.
//

import Foundation

extension String{
    var comaSeparatedStrings:[String]{
        get{
            let replaced = String(self.characters.map {
                $0 == "," ? " " : $0
            })
            var array = [String]()
            array = replaced.characters.split(separator: " ").map { String($0) }
            
            return array
        }
    }
}

extension Array{
    
    var comaSeparatedString:String{
        get{
            var index = 0
            var string = ""
            for item in self {
                if item as! String == ""{
                    continue
                }
                if index == 0 {
                    string.append(item as! String)
                }
                else{
                    string.append(", " + "\(item as! String)")
                }
                index += 1
            }
            return string
        }
    }
}
