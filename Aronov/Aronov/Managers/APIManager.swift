//
//  APIManager.swift
//  Aronov
//
//  Created by Eugene Aronov on 14.10.16.
//  Copyright © 2016 Eugene Aronov. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class APIManager: NSObject {
    static let sharedInstance = APIManager()

    private let urlForAPI = "http://api.kinopoisk.cf/"
    private let defManager = DefaultsManager.sharedInstance
    
    func getFilmsInRent(date: Date,completion: @escaping ([FilmModel]?) ->()){
        toggleIndicator()
        let method = "getTodayFilms"
        let dtFormatter = DateFormatter()
        dtFormatter.dateFormat = "dd.MM.yyyy"
        let url = urlForAPI + method + "?date=\(dtFormatter.string(from: date))&cityID=\(defManager.cityID)"
        Alamofire.request(url).validate().response { (responce) in
            guard responce.data != nil else {
                print("got an error during respose: \(responce.error)")
                completion(nil)
                self.toggleIndicator()
                return
            }

            let json = JSON(data: responce.data!)
            let array = json["filmsData"]
            let films = array.map({ (string,json) in
                return FilmModel.map(json: json)
            })
            self.toggleIndicator()
            completion(films)
        }
    }
    
    func getDetailInfoForFilm(film: FilmModel, completion: @escaping ((FilmModel?) ->())) {
        let method = "getFilm"
        let url = urlForAPI + method + "?filmID=\(film.id!)"
        toggleIndicator()
        Alamofire.request(url).validate().response { (responce) in
            guard responce.data != nil else {
                print("got an error during respose: \(responce.error)")
                completion(nil)
                self.toggleIndicator()
                return
            }
            let json = JSON(data: responce.data!)
            film.getDescriptionForFilm(json: json)
            self.toggleIndicator()
            completion(film)
        }
    }
    
    func getSessionsForFilmWith(id: Int, date: Date, completion: @escaping ([CinemaModel]?) ->()) {
        let method = "getSeance"
        let dtFormatter = DateFormatter()
        dtFormatter.dateFormat = "dd.MM.yyyy"
        let url = urlForAPI + method + "?filmID=\(id)&date=\(dtFormatter.string(from: date))&cityID=\(defManager.cityID)"
        toggleIndicator()
        Alamofire.request(url).validate().response { (responce) in
            guard responce.data != nil else {
                print("got an error during respose: \(responce.error)")
                completion(nil)
                self.toggleIndicator()
                return
            }
            let json = JSON(data: responce.data!)
            let array = json["items"]
            let sessions = array.map({ (string,json) in
                return CinemaModel.map(json: json)
            })
            self.toggleIndicator()
            completion(sessions)
        }
    }
    
    func getCities(completion: @escaping ([CityModel]?) ->()){
        let method = "getCountryList"
        let url = urlForAPI + method
        Alamofire.request(url).validate().response { (responce) in
            guard responce.data != nil else {
                print("got an error during respose: \(responce.error)")
                completion(nil)
                return
            }
            let json = JSON(data: responce.data!)
            let array = json["countryData"]
            for country in array{
                let method = "getCityList?countryID=\(country.1["countryID"])"
                let url = self.urlForAPI + method
                Alamofire.request(url).validate().response { (responce) in
                    guard responce.data != nil else {
                        print("got an error during respose: \(responce.error)")
                        completion(nil)
                        return
                    }
                    
                    let json = JSON(data: responce.data!)
                    let array = json["cityData"]
                    let cities = array.map({ (string,data) -> CityModel in
                        let model = CityModel.map(json: data)
                        model.country = json["countryName"].string
                        return model
                    })
                    completion(cities)
                }
            }
        }
    }
    
    func toggleIndicator() {
        UIApplication.shared.isNetworkActivityIndicatorVisible = !UIApplication.shared.isNetworkActivityIndicatorVisible
    }
}
