//
//  DefaultsManager.swift
//  Aronov
//
//  Created by Eugene Aronov on 17.10.16.
//  Copyright © 2016 Eugene Aronov. All rights reserved.
//

import UIKit

class DefaultsManager: NSObject {
    static let sharedInstance = DefaultsManager()
    
    private let kCityId = "CityID"
    var cityID = 0
    
    override init(){
        super.init()
        loadSettings()
    }
    
    private func loadSettings(){
        let defaults = UserDefaults.standard
        cityID = defaults.integer(forKey: kCityId)
    }
    
    func saveSettings(){
        let defaults = UserDefaults.standard
        defaults.set(cityID, forKey: kCityId)
    }

}
