//
//  SessionTableViewCell.swift
//  Aronov
//
//  Created by Yevhenii Aronov on 10/16/16.
//  Copyright © 2016 Eugene Aronov. All rights reserved.
//

import UIKit

class SessionTableViewCell: UITableViewCell {
    static let kReuseIdentifer = "SessionTableViewCell"
    
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    
    var cinema: CinemaModel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
