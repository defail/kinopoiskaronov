//
//  FilmCollectionViewCell.swift
//  Aronov
//
//  Created by Eugene Aronov on 14.10.16.
//  Copyright © 2016 Eugene Aronov. All rights reserved.
//

import UIKit

class FilmCollectionViewCell: UICollectionViewCell {
    static let kReuseIdentifer = "FilmCollectionViewCell"
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    var film = FilmModel()
}
