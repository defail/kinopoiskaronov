//
//  CityModel.swift
//  Aronov
//
//  Created by Yevhenii Aronov on 10/17/16.
//  Copyright © 2016 Eugene Aronov. All rights reserved.
//

import UIKit
import SwiftyJSON

class CityModel: NSObject {

    var name: String?
    var country: String?
    var id: Int?
    
    static func map(json: JSON) -> CityModel{
        let model = CityModel()
        
        model.name = json["cityName"].string
        model.country = json["countryName"].string
        model.id = Int(json["cityID"].string!)
        
        return model
    }
}
