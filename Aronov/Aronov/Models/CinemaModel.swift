//
//  SessionModel.swift
//  Aronov
//
//  Created by Yevhenii Aronov on 10/16/16.
//  Copyright © 2016 Eugene Aronov. All rights reserved.
//

import UIKit
import SwiftyJSON
import CoreLocation

class CinemaModel: NSObject {
    
    var address: String?
    var coordinates = CLLocationCoordinate2D()
    var cinemaName: String?
    var times: [String]?
    
    static func map(json: JSON) -> CinemaModel{
        let model = CinemaModel()
        
        model.address = json["address"].string
        if let lonStr = json["lon"].string, let latStr = json["lat"].string{
            model.coordinates.longitude = Double(lonStr)!
            model.coordinates.latitude = Double(latStr)!
        }
        
        model.cinemaName = json["cinemaName"].string
        model.times = [String]()
        if let seances = json["seance"].array {
            for time in seances{
                if let seance = time["time"].string{
                    if !seance.isEmpty {
                        model.times?.append(seance)
                    }
                }
            }
        }
        
        if let seances = json["seance3D"].array {
            for time in seances{
                if let seance = time["time"].string{
                    if !seance.isEmpty {
                        model.times?.append(seance)
                    }
                }
            }
        }
        
        
        return model
    }
}
