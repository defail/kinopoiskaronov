//
//  FilmModel.swift
//  Aronov
//
//  Created by Eugene Aronov on 14.10.16.
//  Copyright © 2016 Eugene Aronov. All rights reserved.
//

import UIKit
import SwiftyJSON

class FilmModel: NSObject {
    
    var id: Int?
    var name: String?
    var year: Int?
    var genres = [""]
    var filmDescription: String?
    var country: String?
    var posterURL: URL?
    var rating: Float = 0
    
    static func map(json: JSON) -> FilmModel{
        let model = FilmModel()
        
        model.id = Int(json["id"].string ?? "")
        model.name = json["nameRU"].string
        model.year = Int(json["year"].string!)
        model.country = json["country"].string
        model.posterURL = URL(string: "https://st.kp.yandex.net/images/film_big/\(model.id!).jpg")
        model.genres = json["genre"].string?.comaSeparatedStrings ?? [""]
        
        // rating responce from server like that 7.1 (1 245)
        let str = json["rating"].string ?? ""
        if let index = str.range(of: " ", options: .numeric)?.lowerBound{
            model.rating = Float(str.substring(to: index))!
        }
        return model
    }
    
    func getDescriptionForFilm(json: JSON) {
        filmDescription = json["description"].string
    }
}
