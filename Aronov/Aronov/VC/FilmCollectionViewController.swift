//
//  FilmCollectionViewController.swift
//  Aronov
//
//  Created by Eugene Aronov on 14.10.16.
//  Copyright © 2016 Eugene Aronov. All rights reserved.
//

import UIKit
import Kingfisher
import DropDown

class FilmCollectionViewController: UICollectionViewController {
    private let dataManager = APIManager.sharedInstance
    private let defManager = DefaultsManager.sharedInstance
    
    private var arrayOfFilms = [FilmModel]()
    private var filteredArray = [FilmModel]()
    let dropDown = DropDown()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if defManager.cityID == 0 {
            navigationController?.pushViewController(CitySearchViewController(), animated: true)
        }
        else{
            getData()
        }
        navigationItem.rightBarButtonItem = UIBarButtonItem.init(title: "Выбрать город", style: .plain, target: self, action: #selector(FilmCollectionViewController.changeCity))
        
        NotificationCenter.default.addObserver(self, selector: #selector(FilmCollectionViewController.cityWasChanged), name: NSNotification.Name(rawValue: CitySearchViewController.kCityWasChoosen), object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getData() {
        dataManager.getFilmsInRent(date: Date.init(timeIntervalSinceNow: 0)) { (array) in
            if array != nil{
                self.arrayOfFilms = array!
                self.filteredArray = array!
                self.collectionView?.reloadData()
            }
            self.getAllGenres()
        }
    }
    
    func getAllGenres() {
        //Creating array for dropdown
        var genres: Set<String> = []
        for film in arrayOfFilms{
            for genre in film.genres{
                if !genre.isEmpty{
                    genres.insert(genre)
                }
            }
        }
        var arrayOfGenres = Array(genres)
        arrayOfGenres.insert("По рейтингу", at: 0)
        arrayOfGenres.insert("Все жанры", at: 1)
        
        dropDown.dataSource = arrayOfGenres
        navigationItem.leftBarButtonItem = UIBarButtonItem.init(title: "Сортировать по", style: .plain, target: self, action: #selector(FilmCollectionViewController.toggleDropDown))
        dropDown.anchorView = navigationItem.leftBarButtonItem
        
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            if index == 0 {
                self.filteredArray.sort(by: { (film1, film2) -> Bool in
                    if Float(film1.rating) > Float(film2.rating){
                        return true
                    }
                    else{
                        return false
                    }
                })
                self.collectionView?.reloadData()
            }
            if index == 1 {
                self.filteredArray = self.arrayOfFilms
                self.collectionView?.reloadData()
                
            }
            if index > 1 {
                self.sortArrayByGenre(genre: item)
            }
        }
    }

    func toggleDropDown(){
        if dropDown.isHidden{
            dropDown.show()
        }
        else{
            dropDown.hide()
        }
    }
    
    func changeCity(){
        navigationController?.pushViewController(CitySearchViewController(), animated: true)
    }
    
    func sortArrayByGenre(genre: String){
        filteredArray = [FilmModel]()
        for film in arrayOfFilms{
            if film.genres.contains(genre){
                filteredArray.append(film)
            }
        }
        collectionView?.reloadData()
    }
    
    // MARK: = Notifications
    
    func cityWasChanged(){
        getData()
    }
    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toDetailVC" {
            if let cell = sender as? FilmCollectionViewCell {
                let vc = segue.destination as! DetailViewController
                vc.film = cell.film
            }
        }
    }
 

    // MARK: UICollectionViewDataSource
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return filteredArray.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: FilmCollectionViewCell.kReuseIdentifer, for: indexPath) as! FilmCollectionViewCell
        
        let film = filteredArray[indexPath.row]

        cell.imageView.kf.setImage(with: film.posterURL!)
        cell.nameLabel.text = film.name
        cell.film = film
        return cell
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}

    // MARK: UICollectionViewDelegateFlowLayout

extension FilmCollectionViewController: UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width / 2 - 16, height: view.frame.height / 2.5)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.init(top: 8, left: 8, bottom: 8, right: 8)
    }

}
