//
//  DetailViewController.swift
//  Aronov
//
//  Created by Eugene Aronov on 14.10.16.
//  Copyright © 2016 Eugene Aronov. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
    var film = FilmModel()
    private let dataManager = APIManager.sharedInstance
    
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var countryLabel: UILabel!
    @IBOutlet weak var yearLabel: UILabel!
    @IBOutlet weak var genresLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var posterView: UIImageView!
    @IBOutlet weak var ratingLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getData()
    }
    
    func getData() {
        dataManager.getDetailInfoForFilm(film: film) { (film) in
            self.fillElements(film: film!)
        }
    }
    
    func fillElements(film: FilmModel) {
        descriptionLabel.text = film.filmDescription
        countryLabel.text = film.country
        yearLabel.text = String(describing: film.year!)
        nameLabel.text = film.name
        posterView.kf.setImage(with: film.posterURL!)
        genresLabel.text = film.genres.comaSeparatedString
        ratingLabel.text = String(film.rating.nextUp)
        
        navigationItem.title = film.name
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toSessionsVC"{
            let vc = segue.destination as! SessionsViewController
            vc.film = film
        }
    }
    

}
