//
//  MapViewController.swift
//  Aronov
//
//  Created by Eugene Aronov on 17.10.16.
//  Copyright © 2016 Eugene Aronov. All rights reserved.
//

import UIKit
import MapKit

class MapViewController: UIViewController {

    @IBOutlet weak var map: MKMapView!
    var cinema = CinemaModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        let mark = MKPointAnnotation.init()
        mark.coordinate = cinema.coordinates
        map.addAnnotation(mark)
        
        map.camera.centerCoordinate = cinema.coordinates
        map.camera.altitude = 800
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
