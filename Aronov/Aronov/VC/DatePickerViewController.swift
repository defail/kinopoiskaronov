//
//  DatePickerViewController.swift
//  Aronov
//
//  Created by Eugene Aronov on 18.10.16.
//  Copyright © 2016 Eugene Aronov. All rights reserved.
//

import UIKit

class DatePickerViewController: UIViewController {
    
    static let kDateWasChoosen = "DateWasChoosen"
    @IBOutlet weak var datePicker: UIDatePicker!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = UIColor.white
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: DatePickerViewController.kDateWasChoosen), object: self, userInfo: ["date": datePicker.date])
    }
}
