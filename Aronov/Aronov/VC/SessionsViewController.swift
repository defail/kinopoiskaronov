//
//  SessionsViewController.swift
//  Aronov
//
//  Created by Yevhenii Aronov on 10/16/16.
//  Copyright © 2016 Eugene Aronov. All rights reserved.
//

import UIKit
import MapKit

class SessionsViewController: UITableViewController {

    var film = FilmModel()
    var arrayOfCinemas = [CinemaModel]()
    var filteredArray = [CinemaModel]()
    
    private let dataManager = APIManager.sharedInstance
    let searchController = UISearchController(searchResultsController: nil)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getDataForDate(date: Date.init(timeIntervalSinceNow: 0))
        setUpSearchController()
        
        NotificationCenter.default.addObserver(self, selector: #selector(SessionsViewController.dateWasChanged(notification:)), name: NSNotification.Name(rawValue: DatePickerViewController.kDateWasChoosen), object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setUpSearchController() {
        searchController.searchResultsUpdater = self
        searchController.hidesNavigationBarDuringPresentation = true
        searchController.dimsBackgroundDuringPresentation = false
        definesPresentationContext = true
        tableView.tableHeaderView = searchController.searchBar
        searchController.searchBar.placeholder = "Выберите кинотеатр"
    }
    
    func getDataForDate(date: Date){
        let dtFormatter = DateFormatter()
        dtFormatter.dateFormat = "dd.MM.yyyy"
        navigationItem.title = dtFormatter.string(from: date)
        dataManager.getSessionsForFilmWith(id: film.id!, date: date) { (cinemas) in
            if cinemas != nil{
                self.arrayOfCinemas = cinemas!
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    // MARK: - Notification
    
    func dateWasChanged(notification: Notification) {
        let date = notification.userInfo?["date"] as! Date
        getDataForDate(date: date)
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toMapVC" {
            if let cell = sender as? SessionTableViewCell {
                let vc = segue.destination as! MapViewController
                vc.cinema = cell.cinema!
            }
        }
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        if searchController.isActive && searchController.searchBar.text != "" {
            return filteredArray.count
        }
        return arrayOfCinemas.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchController.isActive && searchController.searchBar.text != "" {
            return filteredArray[section].times!.count
        }
        return arrayOfCinemas[section].times!.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: SessionTableViewCell.kReuseIdentifer, for: indexPath) as! SessionTableViewCell

        var cinema: CinemaModel?
        
        if searchController.isActive && searchController.searchBar.text != "" {
            cinema = filteredArray[indexPath.section]
        } else {
            cinema = arrayOfCinemas[indexPath.section]
        }

        
        cell.nameLabel.text = cinema?.cinemaName
        cell.addressLabel.text = cinema?.address
        cell.timeLabel.text = cinema?.times?[indexPath.row]
        cell.cinema = cinema

        return cell
    }

}

//MARK: - UISearchResultsUpdating
extension SessionsViewController: UISearchResultsUpdating{
    func updateSearchResults(for searchController: UISearchController){
        filterContentForSearchText(searchText: searchController.searchBar.text!)
    }
    
    func filterContentForSearchText(searchText: String, scope: String = "All") {
        filteredArray = arrayOfCinemas.filter { cinema in
            return (cinema.cinemaName?.lowercased().contains(searchText.lowercased()))!
        }
        
        tableView.reloadData()
    }
}
