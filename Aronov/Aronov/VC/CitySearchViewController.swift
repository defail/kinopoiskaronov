//
//  CitySearchViewController.swift
//  Aronov
//
//  Created by Eugene Aronov on 17.10.16.
//  Copyright © 2016 Eugene Aronov. All rights reserved.
//

import UIKit


class CitySearchViewController: UITableViewController {

    let reuseIdentifer = "TableView"
    let searchController = UISearchController(searchResultsController: nil)
    static let kCityWasChoosen = "CityWasChoosen"
    
    private let dataManager = APIManager.sharedInstance
    private let defManager = DefaultsManager.sharedInstance
    
    var arrayOfCities = [CityModel]()
    var filteredArray = [CityModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getData()
        setUpSearchController()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setUpSearchController() {
        searchController.searchResultsUpdater = self
        searchController.hidesNavigationBarDuringPresentation = true
        searchController.dimsBackgroundDuringPresentation = false
        definesPresentationContext = true
        tableView.tableHeaderView = searchController.searchBar
        searchController.searchBar.placeholder = "Выберите ваш город"
    }
    
    func getData() {
        dataManager.getCities { (cities) in
            if cities != nil{
                for city in cities!{
                    self.arrayOfCities.append(city)
                }
                self.tableView.reloadData()
            }
        }
        
    }
    
    //MARK: - UITableViewDataSource
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredArray.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifer)
        
        if cell == nil{
            cell = UITableViewCell.init(style: .default, reuseIdentifier: reuseIdentifer)
        }
        
        let city = filteredArray[indexPath.row]
        cell?.textLabel?.text = "\(city.name!), \(city.country!)"
        
        return cell!
    }
    
    
    //MARK: - UITableViewDelegate
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        defManager.cityID = filteredArray[indexPath.row].id!
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: CitySearchViewController.kCityWasChoosen), object: nil)
        _ = navigationController?.popViewController(animated: true)
    }
    
}

//MARK: - UISearchResultsUpdating
extension CitySearchViewController: UISearchResultsUpdating{
    func updateSearchResults(for searchController: UISearchController){
        filterContentForSearchText(searchText: searchController.searchBar.text!)
    }
    
    func filterContentForSearchText(searchText: String, scope: String = "All") {
        filteredArray = arrayOfCities.filter { city in
            return (city.name?.lowercased().contains(searchText.lowercased()))! || (city.country?.lowercased().contains(searchText.lowercased()))!
        }
        
        tableView.reloadData()
    }
}
